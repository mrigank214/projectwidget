package com.example.newswidget;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.newswidget.Model4.Articles;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter1 extends FirebaseRecyclerAdapter<Articles,Adapter1.ViewHolder> implements View.OnClickListener   {

    Context context;
    List<Articles> articles;
   /* *//*public Adapter1(Context context,@NonNull FirebaseRecyclerOptions<Articles> options,List<Articles> articles) {

    }*/


    public Adapter1( FirebaseRecyclerOptions<Articles> options, List<Articles> articles,Context context) {
        super(options);
        this.context = context;
        this.articles = articles;
    }


    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Articles model) {

        final Articles a = articles.get(position);

        String imageUrl = a.getUrlToImage();
        String url = a.getUrl();

        Picasso.get().load(imageUrl).into(holder.imageView);

        holder.tvTitle.setText(a.getTitle());
        holder.tvSource.setText(a.getSource().getName());
        holder.tvDate.setText("\u2022"+dateTime(a.getPublishedAt()));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detailed1.class);
                intent.putExtra("title",a.getTitle());
                intent.putExtra("source",a.getSource().getName());
                intent.putExtra("time",dateTime(a.getPublishedAt()));
                intent.putExtra("desc",a.getDescription());
                intent.putExtra("imageUrl",a.getUrlToImage());
                intent.putExtra("url",a.getUrl());
                context.startActivity(intent);
            }
        });

        holder.name.setText(String.valueOf(model.getOrder()));
        holder.course.setText(model.getCreated_at());
        holder.email.setText(model.getType());
        Glide.with(holder.img.getContext()).load(model.getImage_url()).into(holder.img);

        // condition 1
        if (model.getOrder() == 1005) {
            //Glide.with(holder.img.getContext()).load(R.drawable.profile).into(holder.img);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), MainActivity7.class);
                    view.getContext().startActivity(intent);
                }
            });

        }
        // condition 2
        if (model.getOrder() == 2 || model.getOrder() == 6 || model.getOrder() == 1002) {
            Glide.with(holder.img.getContext()).load(R.drawable.vids).into(holder.img);
            holder.email.setText("Video Library");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), MainActivity2.class);
                    view.getContext().startActivity(intent);
                }
            });

        }
        // condition 3
        if (model.getOrder() == 8) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), BannerActivity2.class);
                    view.getContext().startActivity(intent);
                }
            });

        }
        // condition 4
        if (model.getOrder() == 4) {
            // Glide.with(holder.img.getContext()).load(R.drawable.scratch).into(holder.img);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), MainActivity4.class);
                    view.getContext().startActivity(intent);
                }
            });

        }
        // condition 5
        if (model.getOrder() == 12) {
            Glide.with(holder.img.getContext()).load(R.drawable.wheelgo).into(holder.img);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), MainActivity3.class);
                    view.getContext().startActivity(intent);
                }
            });

        }
        // condition 6
        if (model.getOrder() == 11) {
            // Glide.with(holder.img.getContext()).load(R.drawable.birdgame).into(holder.img);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), MainActivity6.class);
                    view.getContext().startActivity(intent);
                }
            });

        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.items,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public void onClick(View v) {
        Intent intent =new Intent(v.getContext(),MainActivity7.class);
        v.getContext().startActivity(intent);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle,tvSource,tvDate;
        ImageView imageView;
        CardView cardView;

        CardView cv;
        CircleImageView img;
        TextView name,course,email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSource = itemView.findViewById(R.id.tvSource);
            tvDate = itemView.findViewById(R.id.tvDate);
            imageView = itemView.findViewById(R.id.imageView);
            cardView = itemView.findViewById(R.id.cardView);

            img=(CircleImageView)itemView.findViewById(R.id.img1);
            name=(TextView)itemView.findViewById(R.id.nametext);
            course=(TextView)itemView.findViewById(R.id.coursetext);
            email=(TextView)itemView.findViewById(R.id.emailtext);
            cv=itemView.findViewById(R.id.cv);


        }
    }


    public String dateTime(String t){
        PrettyTime prettyTime = new PrettyTime(new Locale(getCountry()));
        String time = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:", Locale.ENGLISH);
            Date date = simpleDateFormat.parse(t);
            time = prettyTime.format(date);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return time;

    }

    public String getCountry(){
        Locale locale = Locale.getDefault();
        String country = locale.getCountry();
        return country.toLowerCase();
    }
}