package com.example.newswidget;

public class mymodel {

    String action_url,active,created_at,created_by,description,id,image_url,title,type,updated_at,updated_by;
    int order;

    mymodel(){

    }

    public mymodel(String action_url, String active, String created_at, String created_by, String description, String id, String image_url, String title, String type, String updated_at, String updated_by, int order) {
        this.action_url = action_url;
        this.active = active;
        this.created_at = created_at;
        this.created_by = created_by;
        this.description = description;
        this.id = id;
        this.image_url = image_url;
        this.title = title;
        this.type = type;
        this.updated_at = updated_at;
        this.updated_by = updated_by;
        this.order = order;
    }

    public String getAction_url() {
        return action_url;
    }

    public void setAction_url(String action_url) {
        this.action_url = action_url;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
